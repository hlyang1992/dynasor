#!/bin/sh

DYNASOR=dynasor

K_MAX=500      # Consider kspace from gamma (0) to K_MAX inverse nanometer
K_BINS=150      # Collect result using K_BINS "bins" between 0 and $K_MAX
MAX_FRAMES=5000   # Read at most MAX_FRAMES frames from trajectory file (then stop)
K_POINTS=40000

TRAJECTORY="lammpsrun/dump/dumpT1400.NVT.atom.velocity"
OUTPUT="outputs/dynasor_outT1400_static"

${DYNASOR} -f "$TRAJECTORY" \
    --k-bins=$K_BINS \
	--k-max=$K_MAX \
	--max-frames=$MAX_FRAMES \
	--max-k-points=$K_POINTS \
    --om=$OUTPUT.m




