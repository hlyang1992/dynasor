clear all
close all
clc

%
% Plotting script for NaCl dynasor data.
% Takes a few second to plot everything.
%


%% Figure style

LW=4.0; %Linewidth
MS=4.0;
fontstyle='Times';
fontsize=20;

% Colors
red=[0.9 0.3 0.3];
blue=[0.3 0.3 0.9];
black=[0.1 0.1 0.1];
green=[0.3 0.9 0.3];
purple=[0.8 0.4 0.8];

%% Load data

hbar=1.05457173*1e-34; % m2 kg / s
J2ev=6.24150934e18;

% Load data file
dynasor_out_old

w=hbar*w*J2ev*1000*1e15; %meV

%% Compute functions from the partial functions.

% Static
S_NaNa = F_k_t_0_0(1,:);
S_ClCl = F_k_t_1_1(1,:);
S_NaCl = F_k_t_0_1(1,:);
S_ZZ = S_NaNa + S_ClCl - 2*S_NaCl;
S_NN = S_NaNa + S_ClCl + 2*S_NaCl;

% Dynamic
S_NaNa_kw = S_k_w_0_0;
S_ClCl_kw = S_k_w_1_1;
S_NaCl_kw = S_k_w_0_1;

m_Na=22.990;
m_Cl=35.45;

F_ZZ = F_k_t_0_0 + F_k_t_1_1 - 2*F_k_t_0_1;
F_NN = F_k_t_0_0 + F_k_t_1_1 + 2*F_k_t_0_1;
F_MM = ( m_Na^2 *F_k_t_0_0 + m_Cl^2 * F_k_t_1_1 + 2*m_Na*m_Cl*F_k_t_0_1  ) /(m_Na+m_Cl)^2;

S_ZZ_kw = S_NaNa_kw + S_ClCl_kw - 2*S_NaCl_kw;
S_NN_kw = S_NaNa_kw + S_ClCl_kw + 2*S_NaCl_kw;
S_MM_kw = ( m_Na^2 *S_NaNa_kw + m_Cl^2 * S_ClCl_kw + 2*m_Na*m_Cl*S_NaCl_kw ) /(m_Na+m_Cl)^2;


% Currents
Cl_NaNa_kw = Cl_k_w_0_0;
Cl_ClCl_kw = Cl_k_w_1_1;
Cl_NaCl_kw = Cl_k_w_0_1;

Ct_NaNa_kw = Ct_k_w_0_0;
Ct_ClCl_kw = Ct_k_w_1_1;
Ct_NaCl_kw = Ct_k_w_0_1;


Cl_ZZ_kw=Cl_k_w_0_0+Cl_k_w_1_1-2*Cl_k_w_0_1;
Ct_ZZ_kw=Ct_k_w_0_0+Ct_k_w_1_1-2*Ct_k_w_0_1;

Cl_NN_kw=Cl_k_w_0_0+Cl_k_w_1_1+2*Cl_k_w_0_1;
Ct_NN_kw=Ct_k_w_0_0+Ct_k_w_1_1+2*Ct_k_w_0_1;

Cl_MM_kw = ( m_Na^2*Cl_k_w_0_0 + m_Cl^2*Cl_k_w_1_1 + 2*m_Na*m_Cl*Cl_k_w_0_1 ) /(m_Na+m_Cl)^2;
Ct_MM_kw = ( m_Na^2*Ct_k_w_0_0 + m_Cl^2*Ct_k_w_1_1 + 2*m_Na*m_Cl*Ct_k_w_0_1 ) /(m_Na+m_Cl)^2;


Cl_ZZ_kt = Cl_k_t_0_0+Cl_k_t_1_1-2*Cl_k_t_0_1;
Ct_ZZ_kt = Ct_k_t_0_0+Ct_k_t_1_1-2*Ct_k_t_0_1;

Cl_NN_kt = Cl_k_t_0_0+Cl_k_t_1_1+2*Cl_k_t_0_1;
Ct_NN_kt = Ct_k_t_0_0+Ct_k_t_1_1+2*Ct_k_t_0_1;

Cl_MM_kt = ( m_Na^2*Cl_k_t_0_0 + m_Cl^2*Cl_k_t_1_1 + 2*m_Na*m_Cl*Cl_k_t_0_1 ) /(m_Na+m_Cl)^2;
Ct_MM_kt = ( m_Na^2*Ct_k_t_0_0 + m_Cl^2*Ct_k_t_1_1 + 2*m_Na*m_Cl*Ct_k_t_0_1 ) /(m_Na+m_Cl)^2;


%% Partial S(k,w) Time and frequency

wMax=50;
[tmp wInd]=min(abs(w-wMax));

kind1=10;
kind2=25;

tlim=[0 8000];
wlim=[0 w(wInd)];

figure('Position',[100 100,2200,850],'Color','w')

%NaNa
subplot(2,3,1)
plot(t,F_k_t_0_0(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_k_t_0_0(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.02 0.1])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
ylabel('F(k,t)')
title('NaNa')

subplot(2,3,4)
plot(w(1:wInd),abs(S_NaNa_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_NaNa_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
ylabel('S(k,\omega)')
xlim(wlim)
ylim([min([abs(S_NaNa_kw(1:wInd,kind1));abs(S_NaNa_kw(1:wInd,kind2))]), 1000])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')

%ClCl
subplot(2,3,2)
plot(t,F_k_t_1_1(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_k_t_1_1(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.02 0.1])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
title('ClCl')

subplot(2,3,5)
plot(w(1:wInd),abs(S_ClCl_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_ClCl_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([min([abs(S_ClCl_kw(1:wInd,kind1));abs(S_ClCl_kw(1:wInd,kind2))]), 1000])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')

%NaCl
subplot(2,3,3)
plot(t,F_k_t_0_1(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_k_t_0_1(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.02 0.1])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
title('NaCl')

subplot(2,3,6)
plot(w(1:wInd),abs(S_NaCl_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_NaCl_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([min([abs(S_NaCl_kw(1:wInd,kind1));abs(S_NaCl_kw(1:wInd,kind2))]), 1000])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')



set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)

%% Mass-charge-number S(k,w) Time and frequency

wMax=50;
[tmp wInd]=min(abs(w-wMax));

kind1=10;
kind2=25;

tlim=[0 8000];
wlim=[0 w(wInd)];

figure('Position',[100 100,2200,850],'Color','w')

%Charge
subplot(2,3,1)
plot(t,F_ZZ(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_ZZ(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.015 0.015])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
ylabel('F(k,t)')
title('Charge-Charge')

subplot(2,3,4)
plot(w(1:wInd),abs(S_ZZ_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_ZZ_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
ylabel('S(k,\omega)')
xlim(wlim)
ylim([0.2*min([abs(S_ZZ_kw(1:wInd,kind1));abs(S_ZZ_kw(1:wInd,kind2))]), 100])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')

%Mass
subplot(2,3,2)
plot(t,F_MM(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_MM(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.01 0.05])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
title('Mass-Mass')

subplot(2,3,5)
plot(w(1:wInd),abs(S_MM_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_MM_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([min([abs(S_MM_kw(1:wInd,kind1));abs(S_MM_kw(1:wInd,kind2))]), 1000])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')

%Number
subplot(2,3,3)
plot(t,F_NN(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,F_NN(:,kind2),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.05 0.25])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('Time [fs]')
title('Number-Number')

subplot(2,3,6)
plot(w(1:wInd),abs(S_NN_kw(1:wInd,kind1)),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),abs(S_NN_kw(1:wInd,kind2)),'Color',blue,'LineWidth',LW)
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind2)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([min([abs(S_MM_kw(1:wInd,kind1));abs(S_MM_kw(1:wInd,kind2))]), 1000])
set(gca, 'YTick', [10.^(-3) 10.^(-2) 10.^(-1) 10.^0 10.^1 10.^2 10.^3 10.^4 10.^5])
set(gca,'Yscale','log')


set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)




%% Partial S(k,w) logged map

kStart=2;
wMax=50;
wStart=1;
[tmp wInd]=min(abs(w-wMax));
wlim=[0 w(wInd)];
klim=[k(kStart) k(end)];

figure('Position',[100 100,2200,700],'Color','w')

subplot(1,3,1)
surf(k(kStart:end),w(wStart:wInd),log(abs(S_NaNa_kw(wStart:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
ylabel('\omega [meV]')
title('NaNa')
xlim(klim)
ylim(wlim)
view([0 90])

subplot(1,3,2)
surf(k(kStart:end),w(wStart:wInd),log(abs(S_ClCl_kw(wStart:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
title('ClCl')
set(gca,'Ytick',[])
xlim(klim)
ylim(wlim)
view([0 90])

subplot(1,3,3)
surf(k(kStart:end),w(wStart:wInd),log(abs(S_NaCl_kw(wStart:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
title('NaCl')
set(gca,'Ytick',[])
xlim(klim)
ylim(wlim)
view([0 90])


s=suptitle('Dynamical structure factors T=1400K');

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)
set(s,'fontsize',28)

%% Charge mass number S(k,w) logged map

kStart=2;
wMax=50;
[tmp wInd]=min(abs(w-wMax));
wlim=[0 w(wInd)];
klim=[k(kStart) k(end)];

figure('Position',[100 100,2200,700],'Color','w')


subplot(1,3,1)
surf(k(kStart:end),w(1:wInd),log(abs(S_ZZ_kw(1:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
ylabel('\omega [meV]')
title('Charge-Charge')
xlim(klim)
ylim(wlim)
view([0 90])

subplot(1,3,2)
surf(k(kStart:end),w(1:wInd),log(abs(S_MM_kw(1:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
title('Mass-Mass')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])

subplot(1,3,3)
surf(k(kStart:end),w(1:wInd),log(abs(S_NN_kw(1:wInd,kStart:end))),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
title('Number-Number')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])

s=suptitle('Dynamical structure factors T=1400K');

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)
set(s,'fontsize',28)


%% Partial C(k,w) Time and frequency

wMax=50;
[tmp wInd]=min(abs(w-wMax));

kind1=10;
tlim=[0 4000];
wlim=[0 w(wInd)];

figure('Position',[100 100,2200,850],'Color','w')

%NaNa
subplot(2,3,1)
plot(t,Cl_k_t_0_0(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_k_t_0_0(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.08 0.08])
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
ylabel('C(k,t)')
title('NaNa')

subplot(2,3,4)
plot(w(1:wInd),Cl_NaNa_kw(1:wInd,kind1),'Color',red,'Linewidth',LW)
hold on
plot(w(1:wInd),Ct_NaNa_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('\omega [meV]')
ylabel('C(k,\omega)')
xlim(wlim)
ylim([0 80])


%ClCl
subplot(2,3,2)
plot(t,Cl_k_t_1_1(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_k_t_1_1(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.15 0.15])
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
title('ClCl')

subplot(2,3,5)
plot(w(1:wInd),Cl_ClCl_kw(1:wInd,kind1),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),Ct_ClCl_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([0 80])

%NaCl
subplot(2,3,3)
plot(t,Cl_k_t_0_1(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_k_t_0_1(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.1 0.1])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
title('NaCl')

subplot(2,3,6)
plot(w(1:wInd),Cl_NaCl_kw(1:wInd,kind1),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),Ct_NaCl_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
plot([0 w(wInd)],[0 0],'--k')
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([-40 60])

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)


%% Mass Charge Number C(k,w) Time and frequency

wMax=50;
[tmp wInd]=min(abs(w-wMax));

kind1=10;
tlim=[0 4000];
wlim=[0 w(wInd)];

figure('Position',[100 100,2200,850],'Color','w')


%Charge
subplot(2,3,1)
plot(t,Cl_ZZ_kt(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_ZZ_kt(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.5 0.5])
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
ylabel('C(k,t)')
title('Charge-Charge')

subplot(2,3,4)
plot(w(1:wInd),Cl_ZZ_kw(1:wInd,kind1),'Color',red,'Linewidth',LW)
hold on
plot(w(1:wInd),Ct_ZZ_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'),'location','northwest')
xlabel('\omega [meV]')
ylabel('C(k,\omega)')
xlim(wlim)
ylim([0 160])


%Mass
subplot(2,3,2)
plot(t,Cl_MM_kt(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_MM_kt(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.05 0.1])
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
title('Mass-Mass')

subplot(2,3,5)
plot(w(1:wInd),Cl_MM_kw(1:wInd,kind1),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),Ct_MM_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([0 70])

%Number
subplot(2,3,3)
plot(t,Cl_NN_kt(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_NN_kt(:,kind1),'Color',blue,'LineWidth',LW)
xlim(tlim)
ylim([-0.15 0.15])
legend(strcat('k=',num2str(k(kind1)),'nm^{-1}'),strcat('k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('Time [fs]')
title('Number-Number')

subplot(2,3,6)
plot(w(1:wInd),Cl_NN_kw(1:wInd,kind1),'Color',red,'LineWidth',LW)
hold on
plot(w(1:wInd),Ct_NN_kw(1:wInd,kind1),'Color',blue,'LineWidth',LW)
plot([0 w(wInd)],[0 0],'--k')
legend(strcat('Longitudinal k=',num2str(k(kind1)),'nm^{-1}'),strcat('Transverse k=',num2str(k(kind1)),'nm^{-1}'))
xlabel('\omega [meV]')
xlim(wlim)
ylim([0 250])

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)



%% Charge mass number Cl Ct C(k,w)  map
 
kStart=12;
wMax=50;
[tmp wInd]=min(abs(w-wMax));
wlim=[0 w(wInd)];
klim=[k(kStart) k(end)];

figure('Position',[100 100,2200,900],'Color','w')

subplot(2,3,1)
surf(k(kStart:end),w(1:wInd),Cl_ZZ_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
ylabel('\omega [meV]')
title('Charge-Charge')
set(gca,'Xtick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.252 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,4)
surf(k(kStart:end),w(1:wInd),Ct_ZZ_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
ylabel('\omega [meV]')
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.262 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,2)
surf(k(kStart:end),w(1:wInd),Cl_MM_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
title('Mass-Mass')
set(gca,'Xtick',[])
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.577 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,5)
surf(k(kStart:end),w(1:wInd),Ct_MM_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.587 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)


subplot(2,3,3)
surf(k(kStart:end),w(1:wInd),Cl_NN_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
title('Number-Number')
set(gca,'Xtick',[])
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.9035 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,6)
surf(k(kStart:end),w(1:wInd),Ct_NN_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.913 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)



%% Partials Cl Ct C(k,w)  map
 
kStart=14;
wMax=50;
[tmp wInd]=min(abs(w-wMax));
wlim=[0 w(wInd)];
klim=[k(kStart) k(end)];

figure('Position',[100 100,2200,900],'Color','w')

subplot(2,3,1)
surf(k(kStart:end),w(1:wInd),Cl_NaNa_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
ylabel('\omega [meV]')
title('NaNa')
set(gca,'Xtick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.252 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,4)
surf(k(kStart:end),w(1:wInd),Ct_NaNa_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
ylabel('\omega [meV]')
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.262 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,2)
surf(k(kStart:end),w(1:wInd),Cl_ClCl_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
title('ClCl')
set(gca,'Xtick',[])
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.577 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,5)
surf(k(kStart:end),w(1:wInd),Ct_ClCl_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.587 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,3)
surf(k(kStart:end),w(1:wInd),Cl_NaCl_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
title('NaCl')
set(gca,'Xtick',[])
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.9035 0.853 0.1 0.1],'String','Longitudinal','BackgroundColor',[1 1 1],'LineWidth',2)

subplot(2,3,6)
surf(k(kStart:end),w(1:wInd),Ct_NaCl_kw(1:wInd,kStart:end),'EdgeColor','none','LineStyle','none')
xlabel('k [nm^{-1}]')
set(gca,'Ytick',[])
ylim(wlim)
xlim(klim)
view([0 90])
annotation('textbox',[0.913 0.396 0.1 0.1],'String','Transverse','BackgroundColor',[1 1 1],'LineWidth',2)


set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)
