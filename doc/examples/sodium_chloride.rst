.. _example_sodium_chloride:

Molten Sodium Chloride
**********************

This example deals with molten sodium chloride in order to illustrate
the analysis of multiple component systems and the treatment of
partial correlation functions. As in the case of liquid alumnium the
analsysi is carried out for a spherically averaged momentum vector
**q** The `born/coul` potential in `lammps` was used with parameters
from Lewis and Singer (1974). The melting point was found to be about
1300 K and the simulations were carried out at 1400 K for 4096 atoms
(:math:`8\times8\times8` conventional unit cells).

.. todo::
   Insert proper reference via `references.bib`.

The partial correlation functions are denoted :math:`S_{AA}(q,\omega)`
where :math:`A` is an atomic species and :math:`S` represents an
arbitray correlation function. We define the number correlation
:math:`S_{NN}(q,\omega)`, mass correlation :math:`S_{MM}(q,\omega)`
and the charge correlation :math:`S_{ZZ}(q,\omega)` as

.. math::

   S_{NN}(q,\omega) = S_{AA}(q,\omega) + S_{BB}(q,\omega) +
   S_{AB}(q,\omega)

   S_{MM}(q,\omega) = ( m_A^2 S_{AA}(q,\omega) + m_B^2
   S_{BB}(q,\omega) + m_A m_B S_{AB}(q,\omega) ) / (m_A+m_B)^2
   
   S_{ZZ}(q,\omega) = q_A^2 S_{AA}(q,\omega) + q_B^2
   S_{BB}(q,\omega) + q_Aq_B S_{AB}(q,\omega),

where :math:`m_A` and :math:`q_A` denote the mass and charge of atom
type :math:`A`, respectively. These are useful in ionized systems
because acoustic type modes show up in the mass correlation and optic
type modes can be seen in the charge correlation. For direct
comparison with with exprimental data one can compute a linear
combination of the partial functions weighted with the appropriate
atomic form factors.


Structure factors: F(q,t) and S(q,w)
====================================

The figure below shows the static structure factors.

.. figure:: figs/NaCl_liquid/structureFactor.png
    :scale: 50 %
    :align: center
    
    Number-number and charge-charge structure factor (left) and
    partial structure factors (right).

All structure factors agree well with Figures 10.2 and 10.3 in
:cite:`HansenMcDonald`. The normalization differs by a factor of two,
which is simply a matter of choice if the normalization for the
partial structure factor should be done with :math:`N_A` or
:math:`N_A+N_B`.

Below the partial intermediate scattering functions and dynamical
structure factors are shown.

.. figure:: figs/NaCl_liquid/S_partial.png
    :scale: 50 %
    :align: center
    
    Partial :math:`F(q,t)` and :math:`S(q,\omega)` for two different
    **q**-values.

.. figure:: figs/NaCl_liquid/S_CMN.png
    :scale: 50 %
    :align: center
    
    Mass charge and number computed from partial :math:`F(q,t)` and
    :math:`S(q,\omega)`. Note the logged scale for :math:`S(q,\omega)`
    in order to resolve some peaks.

.. figure:: figs/NaCl_liquid/S_partial_map.png
    :scale: 50 %
    :align: center
    
    Map of logged partial dynamical structure factors.

.. figure:: figs/NaCl_liquid/S_CMN_map.png
    :scale: 50 %
    :align: center
    
    Map of logged charge, mass and number dynamical structure factors.


Charge correlation: C(q,t) and C(q,w)
=====================================

Below the partial, charge, mass and number longitudinal and transverse
current correlation functions are shown. Note that the peaks in the
frequency domain are much more easily seen compared to the dynamical
structure factors. These figures show four modes (TA, TO, LA, LO).

.. figure:: figs/NaCl_liquid/C_partial.png
    :scale: 50 %
    :align: center
    
    Partial current correlations :math:`C(q,t)` and  :math:`C(q,\omega)`.

.. figure:: figs/NaCl_liquid/C_CMN.png
    :scale: 50 %
    :align: center
    
    Mass, charge and number current correlations :math:`C(q,t)` and
    :math:`C(q,\omega)`.

Below maps of the current correlations are shown.

.. figure:: figs/NaCl_liquid/C_partial_map.png
    :scale: 50 %
    :align: center
    
    Map of partial current correlations.

.. figure:: figs/NaCl_liquid/C_CMN_map.png
    :scale: 50 %
    :align: center
    
    Map of  current, mass and number current correlations.
